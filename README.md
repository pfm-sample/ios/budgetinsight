# BudgetInsight

## Getting started

Before getting started, please ensure that your development environment meets the following requirements:

- Xcode Version 14.3+
- Swift 5.0+
- CocoaPods 5.7.2
- Ruby 2.6.10


## Setup Guide

Follow the steps below to set up the project:

- Open SampleApp project in Xcode.
- Click on project in the Project Navigator.
- Select your target.
- Make sure the General tab is selected.
- Scroll down to “Frameworks, Libraries, and Embedded Content”.
- Drag in desired XCFrameworks from the downloaded SDK. It is wired up automatically as a dependency on your target.


## Note: This project is specific to the Budget Insight Widget.
The project setup is intended for the Budget Insight Widget Only although podspec are define for other widgets with commented code for more information.

## Download Widgets from S3 Bucket 
Download widgets from S3 Bucket and add place in a folder. eg, `ingage sdk`. Local Podspec are required to be install framework via cocoapods.

```ruby
target 'SampleApp' do
  use_frameworks!

  # Spire Util
  pod 'SpireUtil', :path => "./ingage\ sdk/SpireUtil.podspec"

  # Budget Insight Widget
  pod 'BudgetInsightWidget', :path => "./ingage\ sdk/BudgetInsightWidget.podspec"

#   Transaction Insight Widget
#  pod 'TransactionInsightWidget', :path => "./ingage\ sdk/TransactionInsightWidget.podspec"

#   Budget Insight Detail Widget
#   pod 'BudgetInsightDetail', :path => "./ingage\ sdk/BudgetInsightDetail.podspec"

#    Transaction Insight Detail Widget
#   pod 'TIDetails', :path => "./ingage\ sdk/TIDetails.podspec"
end
```

Open terminal and write
```bash
# 1. Navigate to the project directory
cd SampleApp/

# 2. Run the pod install command
pod install
```
