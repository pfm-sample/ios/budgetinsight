//
//  ViewController.swift
//  SampleApp
//
//  Created by Alizain on 08/08/2023.
//

import UIKit
import BudgetInsightWidget

class ViewController: UIViewController {
    
    var budgetInsightWidget: BudgetInsightSliderView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBudgetView()
    }

    func setupBudgetView() {
//        let token = "token"
//        let customerId = "customer-id"
//        let locale = "en"
//        let domain = "https://www.google.com/"
        
        let token = "518e66d25e0a48f1989f4b13927e5c4f"
        let customerId = "9211325"
        let locale = "en"
        let domain = "https://abo.sit.spiretech.dev:30263/"

        budgetInsightWidget = BudgetInsightSliderView.create(token: token,
                                                             customerId: customerId,
                                                             locale: locale,
                                                             domain: domain)

        budgetInsightWidget.set(themeData: ThemeManager.instance.setupTheme())
        
        budgetInsightWidget.didTapOnWidget = {
            // navigate user on tapping budget
        }
        
        budgetInsightWidget.sessionTimedOut = {
            self.view.showAlert( message: "Session time out", buttonText: "OK") {
                // perform session time out specific task
            }
        }
        
        budgetInsightWidget.layer.cornerRadius = 20
        budgetInsightWidget.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(budgetInsightWidget)
        
        let guide = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            budgetInsightWidget.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0),
            budgetInsightWidget.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            budgetInsightWidget.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0),
            budgetInsightWidget.heightAnchor.constraint(equalToConstant: 200)
        ])

    }
}
