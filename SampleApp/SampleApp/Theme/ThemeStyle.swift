//
//  ThemeStyle.swift
//  TransactionInsightDetailApp
//
//  Created by Alizain on 31/08/2023.
//

import UIKit

import SpireUtil

class ThemeManager {
    var theme: ThemeName = .Spire

    enum ThemeName: String {
        case Spire = "SPIRE"
        case KFHB = "KFHB"
    }

    static let instance = ThemeManager()

    private init() {}

    func setupTheme() -> ThemeData {

        print(theme)
        switch theme {
        case .KFHB:
            return KfhbTheme().build()
        default:
            return SpireTheme().build()
        }
    }
}

protocol ThemeBuilder {
    func build() -> ThemeData
}

class SpireTheme: ThemeBuilder {
    func build() -> ThemeData {
        return .spireTheme
    }
}

class KfhbTheme: ThemeBuilder {

    static private let PRIMARY = UIColor(hex: "#003728FF")!

    static private let ON_PRIMARY = UIColor(hex: "#C7C7C7FF")!

    static private let PRIMARY_VARIANT = UIColor(hex: "#008250FF")!

    static private let SURFACE = UIColor(hex: "#EFEEE9FF")!

    static private let SURFACE_2 = UIColor(hex: "#FFFFFFFF")!

    static private let ON_SURFACE = UIColor(hex: "#003728FF")!

    static private let ON_SURFACE_CONTAINER = UIColor(hex: "#5A646EFF")!

    static private let SECONDARY = UIColor(hex: "#BBAA81FF")!

    static private let ON_SECONDARY = UIColor(hex: "#5A646EFF")!

    static private let ERROR = UIColor.red

    static private let ON_TARGET_COLOR = UIColor(hex: "#5A646EFF")!

    static private let OFF_TARGET_COLOR = UIColor(hex: "#CF1322FF")!

    static private let LOW_BUDGET_COLOR = UIColor(hex: "#D48806FF")!

    static private let headlineLarge: TextStyle = {
        return TextStyle(fontFamily: "Inter-Medium",
                         color: PRIMARY,
                         fontSize: 32)
    }()

    static private let headlineMedium: TextStyle = {
        return TextStyle(fontFamily: "Inter-Bold",
                         color: PRIMARY,
                         fontSize: 24)
    }()

    static private let headlineSmall: TextStyle = {
        return TextStyle(fontFamily: "Inter-Medium",
                         color: PRIMARY,
                         fontSize: 20)
    }()

    static private let displayLarge: TextStyle = {
        return TextStyle(fontFamily: "Inter-Bold",
                         color: PRIMARY,
                         fontSize: 18)
    }()

    static private let displayMedium: TextStyle = {
        return TextStyle(fontFamily: "Inter-Bold",
                         color: PRIMARY,
                         fontSize: 16)
    }()

    static private let displaySmall: TextStyle = {
        return TextStyle(fontFamily: "Inter-Bold",
                         color: ON_SECONDARY,
                         fontSize: 12)
    }()

    static private let bodyLarge: TextStyle = {
        return TextStyle(fontFamily: "Inter-Medium",
                         color: PRIMARY,
                         fontSize: 18)
    }()

    static private let bodyMedium: TextStyle = {
        return TextStyle(fontFamily: "Inter-Medium",
                         color: PRIMARY,
                         fontSize: 16)
    }()

    static private let bodySmall: TextStyle = {
        return TextStyle(fontFamily: "Inter-Medium",
                         color: PRIMARY,
                         fontSize: 14)
    }()

    static private let labelLarge: TextStyle = {
        return TextStyle(fontFamily: "Inter-Regular",
                         color: KfhbTheme.PRIMARY_VARIANT,
                         fontSize: 16)
    }()

    static private let labelMedium: TextStyle = {
        return TextStyle(fontFamily: "Inter-Regular",
                         color: PRIMARY,
                         fontSize: 14)
    }()

    static private let labelSmall: TextStyle = {
        return TextStyle(fontFamily: "Inter-Regular",
                         color: PRIMARY,
                         fontSize: 12)
    }()

    static private let pageControlStyle: PageControlStyle = {
        return PageControlStyle(currentPageIndicatorTintColor: PRIMARY,
                                pageIndicatorTintColor: ON_PRIMARY)
    }()

    static private let timeLineListViewStyle: TimeLineListViewStyle = {
        let eventImage = UIImage(named: "section-circle", in: Bundle.main, compatibleWith: nil)
        return TimeLineListViewStyle(timelineEventImage: eventImage,
                                     surface: BoxStyle(color: SURFACE_2),
                                     onSurface: BoxStyle(color: PRIMARY_VARIANT),
                                     surfaceContainer: BoxStyle(color: SURFACE, radius: 6),
                                     lineView: BoxStyle(color: SURFACE),
                                     sectionLabel: bodyMedium,
                                     titleLabel: bodyMedium,
                                     subtitleLabel: displaySmall,
                                     creditLargeLabel: displayLarge.copyWith(color: PRIMARY_VARIANT),
                                     creditSmallLabel: bodySmall.copyWith(color: PRIMARY_VARIANT),
                                     debitLargeLabel: displayLarge.copyWith(color: ON_SECONDARY),
                                     debitSmallLabel: bodySmall.copyWith(color: ON_SECONDARY))
    }()

    static private let monthSliderViewStyle: MonthSliderViewStyle = {
        return  MonthSliderViewStyle(background: .init(color: SURFACE),
                                     onBackground: .init(color: ON_SURFACE),
                                     selectedLabel: bodyMedium,
                                     unselectedLabel: bodySmall)
    }()

    static private let doughnutChartStyle: DoughnutChartStyle = {
        return DoughnutChartStyle(headingCreditLabel: KfhbTheme.bodyMedium,
                                  amountCreditLabel: KfhbTheme.headlineLarge,
                                  amountSmallCreditLabel: KfhbTheme.bodyMedium,
                                  currencyCreditLabel: KfhbTheme.bodyMedium,
                                  headingDebitLabel: KfhbTheme.bodyMedium,
                                  amountDebitLabel: KfhbTheme.headlineLarge,
                                  amountSmallDebitLabel: KfhbTheme.bodyMedium,
                                  currencyDebitLabel: KfhbTheme.bodyMedium,
                                  currentStateLabel: KfhbTheme.bodyMedium,
                                  background: .init(color: KfhbTheme.SURFACE),
                                  moneyInColor: .init(color: KfhbTheme.PRIMARY_VARIANT),
                                  moneyOutColor: .init(color: UIColor(hex: "#BBAA81FF")!))
    }()

    static private let tableViewStyle: TableViewStyle = {
        return TableViewStyle(background: .init(color: KfhbTheme.SURFACE ),
                              surfaceContainer: BoxStyle(color: UIColor(hex: "#FFFFFFFF")!, radius: 6),
                              infoLabel: KfhbTheme.labelLarge.copyWith(color: UIColor(hex: "#5A646EFF")!),
                              descriptionLabel: KfhbTheme.labelLarge.copyWith(color: KfhbTheme.ON_SURFACE),
                              sectionLabel: KfhbTheme.displayLarge)
    }()

    static private let activityIndicatorStyle: ActivityIndicatorStyle = {
        return ActivityIndicatorStyle(indicatorColor: KfhbTheme.PRIMARY,
                                      messageLabel: KfhbTheme.labelMedium)
    }()

    static private let budgetIncomeViewStyle: BudgetIncomeViewStyle = {
        return BudgetIncomeViewStyle(titleLabel: displayMedium,
                                     amountLabel: displayMedium,
                                     currencyLabel: labelSmall,
                                     surfaceContainer: BoxStyle(color: SURFACE_2, radius: 6))
    }()

    static let progressViewStyle: ProgressViewStyle = {
        return .init(progressForegroundColor: .init(color: PRIMARY_VARIANT),
                     progressBackgroundColor: .init(color: SECONDARY),
                     progressErrorColor: .init(color: ERROR),
                     titleLabel: KfhbTheme.bodyMedium,
                     amountLabel: KfhbTheme.headlineLarge,
                     currencyLabel: KfhbTheme.labelMedium,
                     smallCurrencyLabel: KfhbTheme.labelMedium,
                     allocatedLabel: KfhbTheme.bodyMedium.copyWith(color: ON_SECONDARY),
                     surfaceContainer: .init(color: SURFACE_2, radius: 6))
    }()

    static let textFieldStyle: TextFieldStyle = {
        return .init(container: .init(color: SURFACE_2, radius: 6),
                     borderColor: PRIMARY_VARIANT,
                     borderWidth: 1,
                     text: KfhbTheme.bodyMedium.copyWith(color: PRIMARY_VARIANT),
                     cursor: PRIMARY_VARIANT
        )
    }()

    static let budgetViewStyle: BudgetViewStyle = {
        return .init(insightLabel: KfhbTheme.bodyMedium,
                     dateLabel: KfhbTheme.bodyMedium,
                     defaultLabel: KfhbTheme.labelMedium,
                     hintLabel: KfhbTheme.labelMedium,
                     hintBoldLabel: KfhbTheme.bodySmall,
                     sectionTitle: KfhbTheme.headlineMedium,
                     container: .init(color: SURFACE_2, radius: 6),
                     background: .init(color: SURFACE),
                     titleLabel: KfhbTheme.labelLarge.copyWith(color: PRIMARY),
                     descriptionLabel: KfhbTheme.labelSmall,
                     amountLabel: KfhbTheme.labelMedium.copyWith(color: ON_SURFACE_CONTAINER),
                     addButtonLabel: KfhbTheme.labelMedium.copyWith(color: PRIMARY_VARIANT),
                     onTargetLabel: KfhbTheme.labelLarge.copyWith(color: ON_TARGET_COLOR),
                     lowBudgetLabel: KfhbTheme.labelLarge.copyWith(color: LOW_BUDGET_COLOR),
                     offTargetLabel: KfhbTheme.labelLarge.copyWith(color: OFF_TARGET_COLOR)
        )
    }()

    public static let transactionInsightStyle: TransactionInsightStyle = {
        let seeAllLTR = UIImage(named: "see-all-icon", in: Bundle.main, with: nil)
        let seeAllRTL = UIImage(named: "see-all-icon-ar", in: Bundle.main, with: nil)
        return .init(insightLabel: KfhbTheme.bodyMedium,
                     dateLabel: KfhbTheme.bodyMedium,
                     recentTransactionLabel: KfhbTheme.bodyMedium,
                     titleLabel: KfhbTheme.labelLarge.copyWith(color: PRIMARY),
                     descriptionLabel: KfhbTheme.labelMedium,
                     recurringLabel: KfhbTheme.labelSmall,
                     seeAllButtonLabel: KfhbTheme.displayMedium.copyWith(color: PRIMARY_VARIANT),
                     seeAllButtonImageLTR: seeAllLTR,
                     seeAllButtonImageRTL: seeAllRTL,
                     container: .init(color: SURFACE_2, radius: 6))
    }()

    func build() -> ThemeData {
        return ThemeData(pageControlStyle: KfhbTheme.pageControlStyle,
                                timeLineListView: KfhbTheme.timeLineListViewStyle,
                                monthSliderView: KfhbTheme.monthSliderViewStyle,
                                doughnutChart: KfhbTheme.doughnutChartStyle,
                                tableViewStyle: KfhbTheme.tableViewStyle,
                                activityIndicatorStyle: KfhbTheme.activityIndicatorStyle,
                                budgetIncomeViewStyle: KfhbTheme.budgetIncomeViewStyle,
                                progressViewStyle: KfhbTheme.progressViewStyle,
                                textFieldStyle: KfhbTheme.textFieldStyle,
                                budgetViewStyle: KfhbTheme.budgetViewStyle,
                                transactionInsightStyle: KfhbTheme.transactionInsightStyle
               )
    }
}
