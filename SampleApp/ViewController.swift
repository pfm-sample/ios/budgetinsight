//
//  ViewController.swift
//  SampleApp
//
//  Created by Alizain on 08/08/2023.
//

import UIKit
import BudgetInsightWidget

class ViewController: UIViewController {
    
    var budgetInsightWidget: BudgetInsightSliderView!

    let token = "token"
    let customerId = "customer-id"
    let locale = "en"
    let domain = "https://www.google.com/"

    override func viewDidLoad() {
        super.viewDidLoad()
        setupBudgetView()
        // setupTransactionInsight()
    }

    func setupBudgetView() {

        budgetInsightWidget = BudgetInsightSliderView.create(token: token,
                                                             customerId: customerId,
                                                             locale: locale,
                                                             domain: domain)

        budgetInsightWidget.set(themeData: ThemeManager.instance.setupTheme())
        
        budgetInsightWidget.didTapOnWidget = {
            // navigate user on tapping budget
        }
        
        budgetInsightWidget.sessionTimedOut = {
            self.view.showAlert( message: "Session time out", buttonText: "OK") {
                // perform session time out specific task
            }
        }
        
        budgetInsightWidget.layer.cornerRadius = 20
        budgetInsightWidget.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(budgetInsightWidget)
        
        let guide = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            budgetInsightWidget.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0),
            budgetInsightWidget.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            budgetInsightWidget.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0),
            budgetInsightWidget.heightAnchor.constraint(equalToConstant: 200)
        ])

    }
    
    /*
    func setupTransactionInsight() {
        insightWidget = InsightView.create(token: token,
                                                customerId: customerId,
                                                locale: locale,
                                                domain: domain)
        insightWidget.translatesAutoresizingMaskIntoConstraints = false
        insightWidget.set(themeData: ThemeManager.instance.setupTheme())
        insightWidget.backgroundColor = .red
        view.addSubview(insightWidget)

        let guide = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            insightWidget.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0),
            insightWidget.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0),
            insightWidget.topAnchor.constraint(equalTo: budgetInsightWidget.bottomAnchor, constant: 16),
            insightWidget.heightAnchor.constraint(equalToConstant: view.frame.height*0.65)
        ])
    }
    */
}
